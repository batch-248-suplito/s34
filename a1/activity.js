const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

let users = [
    {
        username: "John Doe",
        password: 'john_doe'
    },
    {
        username: "Ben Smith",
        password: 'ben_smith'
    },
    {
        username: 'Anne Curtis',
        password: 'anne_curtis'
    }
];

//get route /home
app.get("/home", (req,res) => {
    res.send('Welcome to the Home Page!');
})

// get route /items
app.get("/items", (req,res) => {
    res.send(users);
})

//delete /delete-item
app.delete("/delete-item", (req, res) => {
    let { username } = req.body;
    if (!users.some((user) => user.username === username)) {
        res.send(`User "${username}" does not exist`);
    }
    users = users.filter((user) => user.username !== username);
    res.send(`User "${username}" has succesfully deleted!`);
});

app.listen(port, () => console.log(`Server is running at port ${port}`));